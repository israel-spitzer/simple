const morgan = require('morgan');
const express = require('express');
const app = express();

const PORT = process.env.PORT || 3000;

// Middleware to log every request
app.use(morgan(':method :url :status :res[content-length] - :response-time ms'));


app.get('/', (req, res) => {
  res.send('Hello, world!');
});

app.listen(PORT, () => {
  console.log(`Server is running on port ${PORT}`);
});
